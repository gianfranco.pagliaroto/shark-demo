import { middyfy } from "@libs/lambda";
import { DynamoDB } from "aws-sdk";

const dynamoDB = new DynamoDB.DocumentClient();

const buildErrorResponse = (errorMessage: string) => {
  return {
    statusCode: 500,
    body: JSON.stringify({
      message: errorMessage,
    }),
  };
};

const shuffleArray = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + Math.random()));
    const temp = a[i];
    a[i] = a[j];
    a[j] = temp;
  }

  return a;
};

export const getTeamMembers = async (event) => {
  try {
    const data = await dynamoDB
      .scan({
        TableName: process.env.DYNAMO_TABLE_TEAM_MEMBER,
      })
      .promise();

    return {
      statusCode: 200,
      body: JSON.stringify({ ...data, Items: shuffleArray(data.Items) }),
    };
  } catch (err) {
    console.log(err);
    return buildErrorResponse(err.message);
  }
};

export const main = middyfy(getTeamMembers);
