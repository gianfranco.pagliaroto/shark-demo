import type { ValidatedEventAPIGatewayProxyEvent } from "@libs/api-gateway";
import { middyfy } from "@libs/lambda";
import { DynamoDB } from "aws-sdk";

import schema from "./schema";
import * as uuid from "uuid";

const dynamoDB = new DynamoDB.DocumentClient();

const buildErrorResponse = (errorMessage: string) => {
  return {
    statusCode: 500,
    body: JSON.stringify({
      message: errorMessage,
    }),
  };
};

const saveTeamMember: ValidatedEventAPIGatewayProxyEvent<
  typeof schema
> = async (event) => {
  console.log(
    `FirstName: ${event.body.firstName}, LastName: ${event.body.lastName}, Team: ${event.body.team}`
  );
  const id = uuid.v1();
  const firstName = event.body.firstName;
  const lastName = event.body.lastName;
  const team = event.body.team;

  try {
    const params = {
      TableName: process.env.DYNAMO_TABLE_TEAM_MEMBER,
      Item: {
        id,
        firstName,
        lastName,
        team,
      },
    };

    await dynamoDB.put(params).promise();
    return {
      statusCode: 200,
      body: JSON.stringify(params.Item),
    };
  } catch (err) {
    console.log(err);
    return buildErrorResponse(err.message);
  }
};

// export const getTeamMembers: ValidatedEventAPIGatewayProxyEvent<
//   typeof schema
// > = async (event) => {
//   try {
//     const data = await dynamoDB
//       .scan({
//         TableName: process.env.DYNAMO_TABLE_TEAM_MEMBER,
//       })
//       .promise();

//     return {
//       statusCode: 200,
//       body: JSON.stringify(data),
//     };
//   } catch (err) {
//     console.log(err);
//     return sendErrorResponse(err.message);
//   }
// };

export const main = middyfy(saveTeamMember);
