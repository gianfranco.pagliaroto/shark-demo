export default {
  type: "object",
  properties: {
    firstName: { type: "string" },
    lastName: { type: "string" },
    team: { type: "string" },
  },
  required: ["firstName", "lastName", "team"],
} as const;
